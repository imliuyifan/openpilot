import struct

import common.numpy_fast as np
from selfdrive.config import Conversions as CV


# *** Honda specific ***
def can_cksum(mm):
  s = 0
  for c in mm:
    c = ord(c)
    s += (c>>4)
    s += c & 0xF
  s = 8-s
  s %= 0x10
  return s

def fix(msg, addr):
  msg2 = msg[0:-1] + chr(ord(msg[-1]) | can_cksum(struct.pack("I", addr)+msg))
  return msg2

def make_can_msg(addr, dat, idx, alt):
  if idx is not None:
    dat += chr(idx << 4)
    dat = fix(dat, addr)
  return [addr, 0, dat, alt]

# def create_brake_command(apply_brake, pcm_override, pcm_cancel_cmd, chime, idx):
#   """Creates a CAN message for the Honda DBC BRAKE_COMMAND."""
#   pump_on = apply_brake > 0
#   brakelights = apply_brake > 0
#   brake_rq = apply_brake > 0

#   pcm_fault_cmd = False
#   amount = struct.pack("!H", (apply_brake << 6) + pump_on)
#   msg = amount + struct.pack("BBB", (pcm_override << 4) |
#                              (pcm_fault_cmd << 2) |
#                              (pcm_cancel_cmd << 1) | brake_rq, 0x80,
#                              brakelights << 7) + chr(chime) + "\x00"
#   return make_can_msg(0x1fa, msg, idx, 0)

# def create_gas_command(gas_amount, idx):
#   """Creates a CAN message for the Honda DBC GAS_COMMAND."""
#   msg = struct.pack("!H", gas_amount)
#   return make_can_msg(0x200, msg, idx, 0)

# def create_accord_steering_control(apply_steer, idx):
#   # TODO: doesn't work for some reason
#   if apply_steer == 0:
#     dat = [0, 0, 0x40, 0]
#   else:
#     dat = [0,0,0,0]
#     rp = np.clip(apply_steer/0xF, -0xFF, 0xFF)
#     if rp < 0:
#       rp += 512
#     dat[0] |= (rp >> 5) & 0xf
#     dat[1] |= (rp) & 0x1f
#   if idx == 1:
#     dat[0] |= 0x20
#   dat[1] |= 0x20  # always
#   dat[3] = -(dat[0]+dat[1]+dat[2]) & 0x7f

#   # not first byte
#   dat[1] |= 0x80
#   dat[2] |= 0x80
#   dat[3] |= 0x80
#   dat = ''.join(map(chr, dat))

#   return [0,0,dat,8]

def create_steering_control(apply_steer, idx):
#   """Creates a CAN message for the Honda DBC STEERING_CONTROL."""
  commands = []
  msg_0xe4 = struct.pack("!h", apply_steer) + ("\x80\x00" if apply_steer != 0 else "\x00\x00")
  commands.append(make_can_msg(0xe4, msg_0xe4, idx, 1))
  return commands

def create_ui_commands(apply_steer, idx):
  """Creates an iterable of CAN messages for the UIs."""
  commands = []
  return commands
